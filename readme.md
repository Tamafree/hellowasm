# Hello Wasm

This is an example code to get started with webassembly.

## Getting Started

### Install Docker to your local machine

Follow instructions on [this page](https://www.docker.com/get-started).

### Download Emscripten

Download emscripten image by running the folloing command.

```
docker pull trzeci/emscripten
```

### Compile

Compile C to webassembly.

```
docker run --rm -v $(project_path):/src -u emscripten trzeci/emscripten emcc -O3 main.c -o main.wasm
```

### Run

Run index.html using a web server.


## About The Example Code

### C
There are two functions in main.c

* calcInt4096
* calcFloat

Both of them returns a number.

### JavaScript
This is the main part of this project. It does 2 things.
* compile wasm file
* create click event

When you call a module from wasm, don't forget the underscore before module name.
### HTML
Not really doing anything.
