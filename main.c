#include <emscripten.h>

EMSCRIPTEN_KEEPALIVE
int calcInt4096(int x,int y,int opCode) {
  switch (opCode) {
    case 0:
      return (x+y)<<12;
    case 1:
      return (x-y)<<12;
    case 2:
      return (x<<12)*y;
    case 3:
      return (x<<12)/y;
    default:
      return 0;
  }
}

EMSCRIPTEN_KEEPALIVE
float calcFloat(float x,float y,int opCode) {
  switch (opCode) {
    case 0:
      return x+y;
    case 1:
      return x-y;
    case 2:
      return x*y;
    case 3:
      return x/y;
    default:
      return 0;
  }
}
