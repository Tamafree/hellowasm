//compile
fetch("main.wasm")
  .then(response => response.arrayBuffer())
  .then(buffer => WebAssembly.compile(buffer))
  .then(module => {
    return new WebAssembly.Instance(module);
  }).then(instance => {
    setupCalc(instance);
  });

//create click event
function setupCalc(instance){
  const button = document.getElementById('run');
  const num1 = document.getElementById('num1');
  const num2 = document.getElementById('num2');
  const opCode = document.getElementById('op_code');
  const calcType = document.getElementById('calc_type');
  const output = document.getElementById('output');
  const codeList = ["+","-","*","/"];
  let selectedOpCode = null;
  button.value = '=';
  button.addEventListener('click', function() {
    for(let i=0;i<codeList.length;i++){
      if(opCode.value === codeList[i]){
        selectedOpCode = i;
        break;
      }
      if(i == codeList.length){
        alert("error");
        return -1;
      }
    }
    let ans = null;
    if(calcType.value === "int"){
      ans = instance.exports._calcInt4096(num1.value,num2.value,selectedOpCode)/4096;
    }
    else{
      ans = instance.exports._calcFloat(num1.value,num2.value,selectedOpCode);
    }
    output.innerText = ans.toFixed(5);
  }, false);
}
